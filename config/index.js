require('dotenv-flow').config()

module.exports = {
	server: {
		HOSTNAME: process.env.HOSTNAME,
		PORT: process.env.PORT
	},
	database: {
		dbURI: process.env.dbURI,
		poolSize: process.env.poolSize,
		useNewUrlParser: process.env.useNewUrlParser,
		useCreateIndex: process.env.useCreateIndex,
		useUnifiedTopology: process.env.useUnifiedTopology,
		useFindAndModify: process.env.useFindAndModify,
		autoReconnect: process.env.autoReconnect,
		socketTimeoutMS: process.env.socketTimeoutMS,
		connectTimeoutMS: process.env.connectTimeoutMS
	},
	s3storage: {
		bucketName: process.env.bucketName,
		accessKey: process.env.s3accessKey,
		secretKey: process.env.s3secretKey,
		region: process.env.region
	},
	jwt: {
		secretKey: process.env.jwtsecretKey,
		expires: process.env.expires
	},
	twilio: {
		accountSid: process.env.accountSid,
		authToken: process.env.authToken,
		number: process.env.number
	},
	mailer: {
		host: 'smtp.gmail.com',
		port: 587,
		secure: false,
		auth: {
			user: process.env.email,
			pass: process.env.password
		},
		tls: {
			rejectUnauthorized: false
		}
	},
	encryptionKey: process.env.encryptionKey
}
