# Meme-Backend

### Technologies

Meme-Backend uses a number of open source projects to work properly:

- [node.js]
- [Express]
- [MongoDb]

### Installation

Meme-Backend requires [Node.js](https://nodejs.org/) v8+ to run.
Install the dependencies and devDependencies and start the server.

```sh
$ cd Meme-backend
```

Create a .env.local for development, .env.staging for staging, .env.production for production like .env.example

```sh
$ npm install
$ npm run seed
$ npm start
```

For production environments...

```sh
$ NODE_ENV=production
```
