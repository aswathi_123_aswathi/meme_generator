const mongoose = require('mongoose')
const Joi = require('@hapi/joi')

const { Schema } = mongoose

const CatImagesSchema = new Schema(
	{
		name: { type: String },
		isDeleted: { type: Boolean, default: false },
		categoryId: {
			type: Schema.Types.ObjectId,
			required: true,
			ref: 'category'
		},
		path: { type: Array },
		memePath: { type: Array },
		// memeTitle: { type: String },
		imgProperty: { type: String, required: false },
		price: { type: Number, required: false }
	},
	{
		timestamps: true
	}
)

CatImagesSchema.statics.validate = function(body) {
	const schema = {
		name: Joi.string().required(),
		categoryId: Joi.string().required(),
		// memeTitle: Joi.string().required(),
		price: Joi.number().required(),
		path: Joi.array().required()
	}
	return Joi.validate(body, schema, {
		convert: true,
		abortEarly: false,
		allowUnknown: true
	})
}

module.exports = mongoose.model('memeImage', CatImagesSchema)
