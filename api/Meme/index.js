const express = require('express')
const router = express.Router()
const uploadMiddleware = require('../../middlewares/uploadMedia')

const memeController = require('./controller')

router.post('/', uploadMiddleware, memeController.create)
router.get('/count', memeController.getCount)
router.put('/', uploadMiddleware, memeController.update)
router.put('/softDelete', memeController.softDelete)
module.exports = router
