const category = require('../Category/model')
const catImages = require('../CatImages/model')
const settings = require('../../config')
const mongoose = require('mongoose')
const Jimp = require('jimp')
let port = settings.server.PORT
let hostname = settings.server.HOSTNAME

exports.create = async (req, res, next) => {
	try {
		const { body } = req
		const { data } = body
		const dataObj = JSON.parse(data)
		console.log('Log: exports.create -> dataObj', dataObj)
		let imageFile = {}
		let imageFileMeme = {}

		if (!dataObj.categoryId) {
			return res.json({
				success: false,
				message: 'Category required',
			})
		}
		if (!dataObj.imgProperty) {
			return res.json({
				success: false,
				message: 'Image property required',
			})
		}
		if (!dataObj.price) {
			return res.json({
				success: false,
				message: 'Amount required',
			})
		}
		//dataObj.price = parseInt(dataObj.price)
		if (typeof dataObj.price === 'string') {
			dataObj.price = parseFloat(dataObj.price.replace(/,/g, ''))
			console.log('price----', dataObj.price)
		}

		console.log('Log: exports.create -> dataObj', dataObj)
		const categoryData = await category.findById({
			_id: dataObj.categoryId,
			isDeleted: false,
		})

		if (!categoryData) {
			return res.json({
				success: false,
				message: 'Invalid Category',
			})
		}
		if (req.files && Object.keys(req.files).length == 1 && req.files.meme) {
			console.log('req.files---------------', req.files)
			if (req.files.meme[0]) {
				let fileDataMeme = req.files.meme[0]
				console.log(fileDataMeme)
				let imgpath = fileDataMeme.path.replace(/\\/g, '/')
				console.log('imgpath----------------------', imgpath)
				imageFileMeme = {
					title: fileDataMeme.originalname,
					original: imgpath.slice(6),
				}
				console.log('imageFileMeme------------------------', imageFileMeme)
				const original_image =
					'http://' +
					hostname +
					':' +
					port +
					fileDataMeme.path.replace('public', '')
				const logo = 'http://' + hostname + ':' + port + '/logo/waterMark-3.png'
				let destination = './public/uploads/userSubmission/'
				console.log(
					'image----------------------',
					destination + fileDataMeme.filename + '_watermark'
				)
				watermarkImage({ original_image, logo }).then((image) => {
					console.log('inside---------', image)
					image.write(destination + fileDataMeme.filename + '_watermark')
				})
				imageFileMeme.watermark =
					destination.replace('./public', '') +
					fileDataMeme.filename +
					'_watermark'
				thumbImage({ original_image }).then((image) => {
					image.write(destination + fileDataMeme.filename + '_thumb')
				})
				imageFileMeme.thumb =
					destination.replace('./public', '') + fileDataMeme.filename + '_thumb'
				dataObj.memePath = imageFileMeme
			}
			imageFile = {
				title: 'blankImage',
				original: '/img/white-bg.jpg',
				watermark: '/img/white-bg_watermark.jpg',
				thumb: '/img/white-bg_thumb.jpg',
			}
		} else if (
			req.files &&
			Object.keys(req.files).length &&
			req.files.meme &&
			req.files.file
		) {
			let fileData = req.files.file[0]
			if (req.files.meme[0]) {
				let fileDataMeme = req.files.meme[0]
				let imgpath = fileDataMeme.path.replace(/\\/g, '/')
				console.log('imgpath---3--------------', imgpath)
				imageFileMeme = {
					title: fileDataMeme.originalname,
					original: imgpath.slice(6),
				}

				const original_image =
					'http://' +
					hostname +
					':' +
					port +
					fileDataMeme.path.replace('public', '')
				const logo = 'http://' + hostname + ':' + port + '/logo/waterMark-3.png'
				let destination = './public/uploads/userSubmission/'
				console.log(
					"destination + fileDataMeme.filename + '_watermark'",
					destination + fileDataMeme.filename + '_watermark'
				)
				watermarkImage({ original_image, logo }).then((image) => {
					console.log('image-------45', image)
					image.write(destination + fileDataMeme.filename + '_watermark')
				})
				imageFileMeme.watermark =
					destination.replace('./public', '') +
					fileDataMeme.filename +
					'_watermark'
				thumbImage({ original_image }).then((image) => {
					image.write(destination + fileDataMeme.filename + '_thumb')
				})
				imageFileMeme.thumb =
					destination.replace('./public', '') + fileDataMeme.filename + '_thumb'
				dataObj.memePath = imageFileMeme
			}
			let imgpath = fileData.path.replace(/\\/g, '/')
			imageFile = {
				title: fileData.originalname,
				original: imgpath.slice(6),
			}

			const original_image =
				'http://' + hostname + ':' + port + fileData.path.replace('public', '')
			const logo = 'http://' + hostname + ':' + port + '/logo/waterMark-3.png'
			let destination = './public/uploads/userSubmission/'
			watermarkImage({ original_image, logo }).then((image) => {
				image.write(destination + fileData.filename.replace('.', '_watermark.'))
			})
			imageFile.watermark =
				destination.replace('./public', '') +
				fileData.filename.replace('.', '_watermark.')
			thumbImage({ original_image }).then((image) => {
				image.write(destination + fileData.filename.replace('.', '_thumb.'))
			})
			imageFile.thumb =
				destination.replace('./public', '') +
				fileData.filename.replace('.', '_thumb.')
		} else {
			return res.json({
				success: false,
				message: ' File is Required',
				data: {},
			})
		}
		dataObj.path = imageFile
		let meme = await catImages.create(dataObj)
		return res.json({
			success: true,
			message: ' Successfully created',
			data: { meme },
		})
	} catch (e) {
		console.log(e)
		return next(e)
	}
}
exports.update = async (req, res, next) => {
	try {
		const { body } = req
		const { data } = body
		const dataObj = JSON.parse(data)

		let imageFileMeme = {}

		if (!mongoose.Types.ObjectId.isValid(dataObj._id)) {
			return res.json({
				success: false,
				message: 'Meme id required',
			})
		}
		const imageData = await catImages.findById(dataObj._id)
		if (!imageData) {
			return res.json({
				success: false,
				message: 'Invalid Meme',
			})
		}

		if (!dataObj.imgProperty) {
			return res.json({
				success: false,
				message: 'Image property required',
			})
		}
		if (req.files && Object.keys(req.files).length && req.files.meme) {
			if (req.files.meme[0]) {
				let fileDataMeme = req.files.meme[0]
				console.log(fileDataMeme)
				let imgpath = fileDataMeme.path.replace(/\\/g, '/')
				imageFileMeme = {
					title: fileDataMeme.originalname,
					original: imgpath.slice(6),
				}

				const original_image =
					'http://' +
					hostname +
					':' +
					port +
					fileDataMeme.path.replace('public', '')
				const logo = 'http://' + hostname + ':' + port + '/logo/waterMark-3.png'
				let destination = './public/uploads/userSubmission/'

				watermarkImage({ original_image, logo }).then((image) => {
					try {
						image.write(destination + fileDataMeme.filename + '_watermark')
					} catch (error) {}
				})
				imageFileMeme.watermark =
					destination.replace('./public', '') +
					fileDataMeme.filename +
					'_watermark'
				thumbImage({ original_image }).then((image) => {
					try {
						image.write(destination + fileDataMeme.filename + '_thumb')
					} catch (error) {}
				})
				imageFileMeme.thumb =
					destination.replace('./public', '') + fileDataMeme.filename + '_thumb'
				dataObj.memePath = imageFileMeme
			}
		} else {
			return res.json({
				success: false,
				message: ' File is Required',
				data: {},
			})
		}
		const id = dataObj._id
		delete dataObj._id
		let meme = await catImages.findByIdAndUpdate(id, dataObj, {
			new: true,
		})

		return res.json({
			success: true,
			message: ' Successfully created',
			data: { meme },
		})
	} catch (e) {
		console.log(e)
		return next(e)
	}
}
exports.getCount = async (req, res, next) => {
	try {
		const count = await catImages.count({ isDeleted: false })

		return res.json({
			success: true,
			message: 'Meme count Successfully fetched',
			data: {
				count,
			},
		})
	} catch (e) {
		return next(e)
	}
}
exports.softDelete = async (req, res, next) => {
	try {
		const { _id } = req.body
		console.log({ _id })
		if (!mongoose.Types.ObjectId.isValid(_id)) {
			return res.json({
				success: false,
				message: 'Meme id required',
			})
		}
		let meme = await catImages.findByIdAndUpdate(
			{ _id },
			{ isDeleted: true },
			{ new: true }
		)

		return res.json({
			success: true,
			message: ' Successfully updated',
			data: { meme },
		})
	} catch (e) {
		console.log(e)
		return next(e)
	}
}
//Create thumb file
const thumbImage = async (payload) => {
	const [image] = await Promise.all([Jimp.read(payload.original_image)])
	image.scaleToFit(256, Jimp.AUTO, Jimp.RESIZE_BEZIER)
	return image
}
const watermarkImage = async (payload) => {
	try {
		const [image, logo] = await Promise.all([
			Jimp.read(payload.original_image),
			Jimp.read(payload.logo),
		])
		const LOGO_MARGIN_PERCENTAGE = 0.6
		logo.resize(image.bitmap.width / 2.857142857, Jimp.AUTO)

		const xMargin = (image.bitmap.width * LOGO_MARGIN_PERCENTAGE) / 100
		const yMargin = (image.bitmap.width * LOGO_MARGIN_PERCENTAGE) / 100

		const X = image.bitmap.width - logo.bitmap.width - xMargin
		const Y = image.bitmap.height - logo.bitmap.height - yMargin

		return image.composite(logo, X, Y, [
			{
				mode: Jimp.BLEND_SCREEN,
				opacitySource: 0.1,
				opacityDest: 1,
			},
		])
	} catch (error) {}
}
