const transaction = require('../Transactions/model')

exports.getRevenue = async (req, res, next) => {
	try {
		let total = 0
		const revenue = await transaction.aggregate([
			{
				$match: {
					isDeleted: false
				}
			},
			{ $group: { _id: null, total: { $sum: '$amount' } } }
		])
		if (revenue.length) {
			total = revenue[0].total
			console.log('Log: exports.getRevenue -> total', total)
		}
		return res.json({
			success: true,
			message: 'Revenue Successfully fetched',
			data: {
				total
			}
		})
	} catch (e) {
		console.log('Log: exports.getRevenue -> e', e)
		return next(e)
	}
}
