const mongoose = require('mongoose')
const Joi = require('@hapi/joi')

const { Schema } = mongoose

const Transaction = new Schema(
	{
		stripeToken: { type: String, required: true },
		isDeleted: { type: Boolean, default: false },
		paymentMethod: { type: String, required: true },
		fingerprint: { type: String, required: true },
		memeId: {
			type: Schema.Types.ObjectId,
			required: false,
			ref: 'memeImage'
		},
		customerId: { type: String, required: true },
		transactionId: { type: String, required: true },
		amount: { type: Number, required: false }
	},
	{
		timestamps: true
	}
)

Transaction.statics.validate = function(body) {
	const schema = {
		stripeToken: Joi.string().required(),
		categoryId: Joi.string().required(),
		transactionId: Joi.string().required()
	}
	return Joi.validate(body, schema, {
		convert: true,
		abortEarly: false,
		allowUnknown: true
	})
}

module.exports = mongoose.model('transaction', Transaction)
