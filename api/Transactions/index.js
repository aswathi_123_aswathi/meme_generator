const express = require('express')

const router = express.Router()

const transactionController = require('./controller')

router.get('/revenue', transactionController.getRevenue)

module.exports = router
