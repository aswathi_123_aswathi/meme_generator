const mongoose = require('mongoose')
const Joi = require('@hapi/joi')

const { Schema } = mongoose

const FontImagesSchema = new Schema(
	{
		name: { type: String },
		isDeleted: { type: Boolean, default: false },
		path: { type: Array },
	},
	{
		timestamps: true
	}
)

FontImagesSchema.statics.validate = function(body) {
	const schema = {
		name: Joi.string().required(),
		// memeTitle: Joi.string().required(),
		path: Joi.array().required()
	}
	return Joi.validate(body, schema, {
		convert: true,
		abortEarly: false,
		allowUnknown: true
	})
}

module.exports = mongoose.model('font', FontImagesSchema)
