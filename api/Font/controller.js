const category = require('../Category/model')
const font = require('../Font/model')
const settings = require('../../config')
const mongoose = require('mongoose')
const Jimp = require('jimp')
let port = settings.server.PORT
let hostname = settings.server.HOSTNAME

exports.create = async (req, res, next) => {
	try {
		const { body } = req
		const { data } = body
		const dataObj = JSON.parse(data)
		console.log('Log: exports.create -> dataObj', dataObj)
		let imageFile = {}
		

		if (!dataObj.name) {
			return res.json({
				success: false,
				message: 'Name required'
			})
		}
		
		console.log('Log: exports.create -> dataObj', dataObj)
		console.log(req.files);
		if (req.files && Object.keys(req.files).length == 1 && req.files.font) {
			if (req.files.font[0]) {
				let fileDataMeme = req.files.font[0]
				console.log(fileDataMeme)
				let imgpath = fileDataMeme.path.replace(/\\/g, '/')
				imageFileMeme = {
					title: fileDataMeme.originalname,
					original: imgpath.slice(6)
				}
				dataObj.path = imageFileMeme
			}
			let fonts = await font.create(dataObj)
            return res.json({
                success: true,
                message: ' Successfully created',
                data: { fonts }
            })
		} else {
            return res.json({
                success: false,
                message: 'File is required',
            }) 
        }
		
	} catch (e) {
		console.log(e)
		return next(e)
	}
}

exports.getCount = async (req, res, next) => {
	try {
		const count = await font.count({ isDeleted: false })

		return res.json({
			success: true,
			message: 'Font count Successfully fetched',
			data: {
				count
			}
		})
	} catch (e) {
		return next(e)
	}
}
exports.softDelete = async (req, res, next) => {
	try {
        const { _id } = req.body
        console.log( req.body);
		console.log({ _id })
		if (!mongoose.Types.ObjectId.isValid(_id)) {
			return res.json({
				success: false,
				message: 'Font id required'
			})
		}
		let fonts = await font.findByIdAndUpdate(
			{ _id },
			{ isDeleted: true },
			{ new: true }
		)

		return res.json({
			success: true,
			message: ' Successfully updated',
			data: { fonts }
		})
	} catch (e) {
		console.log(e)
		return next(e)
	}
}


exports.getFont = async (req, res, next) => {
	try {
		// const { limit = 20, offset = 0 } = req.query
		const { fonts = '' } = req.query
		let query = { isDeleted: false }
		if (mongoose.Types.ObjectId.isValid(fonts)) {
			query = {
				_id: mongoose.Types.ObjectId(fonts),
				isDeleted: false
			}
		}
		console.log(query)
		const list = await font.find({ ...query }).sort('-createdAt')
		return res.json({
			success: true,
			message: 'All Font Successfully fetched',
			data: {
				list
			}
		})
	} catch (e) {
		console.log('Log: exports.listCatImages -> e', e)
		return next(e)
	}
}

