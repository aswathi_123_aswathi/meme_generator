const express = require('express')
const router = express.Router()
const uploadMiddleware = require('../../middlewares/uploadFiles')

const fontController = require('./controller')

router.post('/', uploadMiddleware, fontController.create)
router.get('/', fontController.getFont)
router.get('/count', fontController.getCount)
// router.put('/', uploadMiddleware, fontController.update)
router.put('/softDelete', fontController.softDelete)
module.exports = router