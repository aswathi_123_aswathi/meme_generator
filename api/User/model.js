const mongoose = require('mongoose')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const {
	jwt: { secretKey, expires },
	s3storage: { bucketName }
} = require('../../config')
const Joi = require('@hapi/joi')

const { Schema } = mongoose

const UserSchema = new Schema(
	{
		firstName: { type: String },
		lastName: { type: String },
		email: {
			type: String,
			unique: true,
			required: true
		},
		password: {
			type: String,
			required: true
		},
		phoneNumber: { type: String },
		age: { type: Number },
		address: {
			city: String,
			state: String,
			zip: String
		},
		role_id: {
			type: Number,
			default: 1
		},
		profilePic: String,
		isDeleted: {
			type: Boolean,
			default: false
		}
	},
	{
		toJSON: {
			transform: function(doc, ret) {
				delete ret.__v
				delete ret.password
				ret.profilePic = ret.profilePic
					? `https://${bucketName}.s3.amazonaws.com/${ret.profilePic}`
					: ''
			}
		},
		timestamps: true
	}
)

UserSchema.statics.validate = function(body) {
	const schema = {
		firstName: Joi.string(),
		lastName: Joi.string(),
		email: Joi.string().required(),
		password: Joi.string().required(),
		phoneNumber: Joi.string().required(),
		age: Joi.number(),
		address: Joi.object()
			.keys({
				city: Joi.string(),
				state: Joi.string(),
				zip: Joi.string()
			})
			.optional(),
		profilePic: Joi.string()
	}

	return Joi.validate(body, schema, {
		convert: true,
		abortEarly: false,
		allowUnknown: true
	})
}

UserSchema.statics.generateHash = async password =>
	bcrypt.hash(password, await bcrypt.genSalt(10))

UserSchema.methods.isValidPassword = async (password, hashedPassword) =>
	await bcrypt.compare(password, hashedPassword)

UserSchema.methods.generateAuthToken = function(rememberMe) {
	let expiresIn = expireIn(1)
	if (rememberMe) expiresIn = expireIn(30)
	return jwt.sign(
		{
			id: this._id
		},
		secretKey,
		{ expiresIn }
	)
}

const expireIn = numDays => {
	const dateObj = new Date()
	return dateObj.setDate(dateObj.getDate() + numDays)
}

module.exports = mongoose.model('adminUser', UserSchema)
