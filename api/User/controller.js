const Amount = require('../DefaultAmount/model')
const Transaction = require('../Transactions/model')
const Category = require('../Category/model')
const CatImages = require('../CatImages/model')
const font = require('../Font/model')
const mongoose = require('mongoose')
const _ = require('lodash')
// const keyPublishable = 'pk_test_sU5rwMi96OllTvOvNC018trx005W8VzMkf'
// const keySecret = 'sk_test_By5qkaiHV37cCUAfboNfEEdT00nY4fLk19'
const keySecret = 'sk_live_6HOez2h8AJsujiYQYL4Cycsk'
const stripe = require('stripe')(keySecret)

exports.charge = async (req, res, next) => {
	try {
		const { stripeEmail, stripeToken, name, memeId } = req.body
		let amount
		console.log(req.body)
		let data = {}
		if (!stripeEmail) {
			return res.json({
				success: false,
				message: 'Email required'
			})
		}
		if (!stripeToken) {
			return res.json({
				success: false,
				message: 'Token required'
			})
		}
		if (!name) {
			return res.json({
				success: false,
				message: 'Name required'
			})
		}
		if (mongoose.Types.ObjectId.isValid(memeId)) {
			const meme = await CatImages.findOne({
				_id: memeId,
				isDeleted: false
			})
			console.log('Log: exports.charge -> meme', meme)
			amount = meme.price
		} else {
			const defaultAmount = await Amount.findOne({})
			amount = defaultAmount.toObject().defaultAmount
		}
		// create a customer
		let customerData = await stripe.customers.create({
			name: name,
			email: stripeEmail, // customer email, which user need to enter while making payment
			source: stripeToken, // token for the given card
			address: {
				line1: 'la',
				postal_code: '98140',
				city: 'San Francisco',
				state: 'CA',
				country: 'US'
			}
		})
		if (customerData && customerData.id) {
			let charged = await stripe.charges.create({
				// charge the customer
				amount: amount * 100,
				description: 'Sample Charge',
				currency: 'usd',
				customer: customerData.id
			})
			console.log(charged)

			if ((charged.status = 'succeeded')) {
				data.transactionId = charged.id
				data.customerId = customerData.id
				data.stripeToken = stripeToken
				data.paymentMethod = charged.payment_method
				data.amount = charged.amount / 100
				data.memeId = memeId
				data.fingerprint = charged.payment_method_details.card.fingerprint

				let result = await Transaction.create(data)
				let catImages = {}
				if (memeId) {
					catImages = await CatImages.find({
						_id: memeId,
						isDeleted: false
					}).populate({ path: 'categoryId', match: { isDeleted: false } })
				}

				if (result._id) {
					return res.send({
						success: true,
						message: 'payment Success',
						data: { catImages }
					})
				}
			} else {
				return res.json({
					success: false,
					message: 'payment Failed',
					data: {}
				})
			}
		} else {
			return res.json({
				success: false,
				message: 'payment Failed',
				data: {}
			})
		}
	} catch (e) {
		console.log(e)
		return next(e)
	}
}

exports.listCatImages = async (req, res, next) => {
	try {
		const { limit = 20, offset = 0 } = req.query
		const { category = '' } = req.query
		let catQuery = {}
		if (mongoose.Types.ObjectId.isValid(category)) {
			catQuery = {
				categoryId: mongoose.Types.ObjectId(category)
			}
		}

		let catImages = await CatImages.find({
			...catQuery,
			isDeleted: false
		})
			.sort('-createdAt')
			.populate({ path: 'categoryId', match: { isDeleted: false } })
			.skip(Number(offset))
			.limit(Number(limit))
		for (let val of catImages) {
			val.path[0] = _.omit(val.path[0], ['original'])
		}
		const memeListCount = await CatImages.countDocuments({
			isDeleted: false,
		});

		const filterCount = await CatImages.countDocuments({
			...catQuery,
			isDeleted: false,
		});
		console.log(memeListCount);
		console.log(filterCount);

		return res.json({
			success: true,
			message: 'All Memes Successfully fetched',
			data: {
				catImages,
				memeListCount,
				filterCount
			}
		})
	} catch (e) {
		console.log('Log: exports.listCatImages -> e', e)
		return next(e)
	}
}
exports.getAllCategory = async (req, res, next) => {
	try {
		// const { limit = 20, offset = 0 } = req.query
		const { category = '' } = req.query
		let catQuery = { isDeleted: false }
		if (mongoose.Types.ObjectId.isValid(category)) {
			catQuery = {
				_id: mongoose.Types.ObjectId(category),
				isDeleted: false
			}
		}
		console.log(catQuery)
		const categoryList = await Category.find({ ...catQuery }).sort('-createdAt')
		console.log(categoryList)
		return res.json({
			success: true,
			message: 'All Category Successfully fetched',
			data: {
				categoryList
			}
		})
	} catch (e) {
		console.log('Log: exports.listCat -> e', e)
		return next(e)
	}
}
exports.getCatCount = async (req, res, next) => {
	try {
		const count = await Category.count({ isDeleted: false })
		console.log('Log: exports.getCount -> user', count)

		return res.json({
			success: true,
			message: 'Category count Successfully fetched',
			data: {
				count
			}
		})
	} catch (e) {
		return next(e)
	}
}
exports.getMemeCount = async (req, res, next) => {
	try {
		const count = await CatImages.count({ isDeleted: false })
		console.log('Log: exports.getCount -> user', count)

		return res.json({
			success: true,
			message: 'Meme count Successfully fetched',
			data: {
				count
			}
		})
	} catch (e) {
		return next(e)
	}
}

exports.listCatImagesById = async (req, res, next) => {
	try {
		
		const { memeId = '' } = req.query
		console.log(memeId);
		if(!memeId) {
			return res.json({
				success: false,
				message: 'Meme id required',
			})
		}
		console.log(memeId);
		let catImages = await CatImages.findOne({
			_id: memeId,
			isDeleted: false,
			price:0
		});
		
		console.log("img------",catImages);
	
		if(catImages) {
			return res.json({
				success: true,
				message: 'All Memes Successfully fetched',
				data: {
					catImages,
				}
			})
		}else {
			return res.json({
				success: false,
				message: 'Access denied',
			})
		}
	

		
	} catch (e) {
		console.log('Log: exports.listCatImages -> e', e)
		return next(e)
	}
}

exports.getFont = async (req, res, next) => {
	try {
		// const { limit = 20, offset = 0 } = req.query
		const { fonts = '' } = req.query
		let query = { isDeleted: false }
		if (mongoose.Types.ObjectId.isValid(fonts)) {
			query = {
				_id: mongoose.Types.ObjectId(fonts),
				isDeleted: false
			}
		}
		console.log(query)
		const list = await font.find({ ...query }).sort('-createdAt')
		return res.json({
			success: true,
			message: 'All Font Successfully fetched',
			data: {
				list
			}
		})
	} catch (e) {
		console.log('Log: exports.listCatImages -> e', e)
		return next(e)
	}
}