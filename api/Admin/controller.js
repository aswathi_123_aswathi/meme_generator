const User = require('../User/model')
const _ = require('lodash')

exports.changepasswordTest = async (req, res, next) => {
	try {
		const {
			body: { oldpassword, password },
			user: { id }
		} = req
		let admin = await User.findById(id).where('isDeleted', false)
		console.log('Log: exports.changepassword -> admin', admin)
		if (!admin) {
			return res.status(400).json({
				success: true,
				message: 'Invalid Token'
			})
		}
		const isOldPasswordMatch = await admin.isValidPassword(
			oldpassword,
			admin.password
		)
		if (!isOldPasswordMatch) {
			return res.json({
				success: false,
				message: 'Password Mismatch. Please check ..',
				data: ''
			})
		}

		const token = await admin.generateAuthToken()
		admin.password = await User.generateHash(password)
		await admin.save()
		admin = admin.toObject()
		admin = _.omit(admin, 'password')
		return res.json({
			success: true,
			message: 'Password Updated Succesfully...',
			data: {
				admin,
				token
			}
		})
	} catch (e) {
		console.log(e)
		return next(e)
	}
}
exports.changepassword = async (req, res, next) => {
	try {
		const {
			body: { password },
			user: { id }
		} = req
		let admin = await User.findById(id).where('isDeleted', false)
		console.log('Log: exports.changepassword -> admin', admin)
		if (!admin) {
			return res.status(400).json({
				success: true,
				message: 'Invalid Token'
			})
		}

		const token = await admin.generateAuthToken()
		admin.password = await User.generateHash(password)
		await admin.save()
		admin = admin.toObject()
		admin = _.omit(admin, 'password')
		return res.json({
			success: true,
			message: 'Password Updated Succesfully...',
			data: {
				admin,
				token
			}
		})
	} catch (e) {
		console.log(e)
		return next(e)
	}
}
