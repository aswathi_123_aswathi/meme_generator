const express = require('express')

const router = express.Router()

const adminController = require('./controller')
const defaultAmountController = require('../DefaultAmount/controller')
router.post('/changePassword', adminController.changepassword)
router.post('/changeDefaultAmount', defaultAmountController.changeDefaultAmount)

module.exports = router
