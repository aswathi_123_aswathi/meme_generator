const User = require('../User/model')

exports.login = async (req, res, next) => {
	try {
		const { email, password } = req.body
		console.log(req.body)
		if (!email || !password) {
			return res.status(422).json({
				success: false,
				message: 'Invalid email or password'
			})
		}

		const user = await User.findOne({
			email,
			role_id: 1,
			isDeleted: false
		})
		// .populate('roleId');

		if (!user) {
			return res.status(422).json({
				success: false,
				message: 'Invalid email or password'
			})
		}

		const validPassword = await user.isValidPassword(password, user.password)
		console.log(validPassword)
		if (!validPassword) {
			return res.status(422).json({
				success: false,
				message: 'Invalid email or password.'
			})
		}
		const token = user.generateAuthToken()

		return res.json({
			success: true,
			isVerified: true,
			message: 'User data fetched successfully',
			data: {
				user,
				token
			}
		})
	} catch (err) {
		console.log('err', err)
		return res.status(500).json({
			success: false,
			message: err.toString()
		})
	}
}
