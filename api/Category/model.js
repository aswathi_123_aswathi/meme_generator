const mongoose = require('mongoose')
const Joi = require('@hapi/joi')

const { Schema } = mongoose

const CategorySchema = new Schema(
	{
		name: { type: String },
		isDeleted: { type: Boolean, default: false }
	},
	{
		timestamps: true
	}
)

CategorySchema.statics.validate = function(body) {
	const schema = {
		name: Joi.string().required()
	}
	return Joi.validate(body, schema, {
		convert: true,
		abortEarly: false,
		allowUnknown: true
	})
}

module.exports = mongoose.model('category', CategorySchema)
