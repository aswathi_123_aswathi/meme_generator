const Category = require('./model')
const CatImages = require('../CatImages/model')

const mongoose = require('mongoose')

exports.create = async (req, res, next) => {
	try {
		const { name } = req.body
		if (req.body.name && Object.keys(req.body).length) {
			let categoryData = await Category.findOne({ name, isDeleted: false })
			if (categoryData) {
				return res.json({
					success: false,
					message: 'Category already exists',
					data: {}
				})
			}
			console.log(categoryData)
			let category = await Category.create(req.body)
			return res.json({
				success: true,
				message: ' Successfully created',
				data: { category }
			})
		} else {
			return res.json({
				success: false,
				message: 'Name is required',
				data: {}
			})
		}
	} catch (e) {
		console.log(e)
		return next(e)
	}
}

exports.getCount = async (req, res, next) => {
	try {
		const count = await Category.count({ isDeleted: false })
		console.log('Log: exports.getCount -> user', count)

		return res.json({
			success: true,
			message: 'Category count Successfully fetched',
			data: {
				count
			}
		})
	} catch (e) {
		return next(e)
	}
}
exports.softDelete = async (req, res, next) => {
	try {
		const { _id } = req.body
		console.log({ _id })
		if (!mongoose.Types.ObjectId.isValid(_id)) {
			return res.json({
				success: false,
				message: 'Meme id required'
			})
		}
		let category = await Category.findByIdAndUpdate(
			{ _id },
			{ isDeleted: true },
			{ new: true }
		)
		let meme = await CatImages.updateMany(
			{ categoryId: _id },
			{ isDeleted: true },
			{ new: true }
		)
		return res.json({
			success: true,
			message: ' Successfully updated',
			data: { category, meme }
		})
	} catch (e) {
		console.log(e)
		return next(e)
	}
}

exports.listCatImages = async (req, res, next) => {
	try {
		const { limit = 20, offset = 0 } = req.query
		const { category = '' } = req.query
		let catQuery = {}
		if (mongoose.Types.ObjectId.isValid(category)) {
			catQuery = {
				categoryId: mongoose.Types.ObjectId(category)
			}
		}

		const catImages = await CatImages.find({
			...catQuery,
			isDeleted: false
		})
			.sort('-createdAt')
			.populate({ path: 'categoryId', match: { isDeleted: false } })
			.skip(Number(offset))
			.limit(Number(limit))

		const memeListCount = await CatImages.countDocuments({
			isDeleted: false,
		});

		const filterCount = await CatImages.countDocuments({
			...catQuery,
			isDeleted: false,
		});
		console.log(memeListCount);
		console.log(filterCount);
		return res.json({
			success: true,
			message: 'All Memes Successfully fetched',
			data: {
				catImages,
				memeListCount,
				filterCount
			}
		})
	} catch (e) {
		console.log('Log: exports.listCatImages -> e', e)
		return next(e)
	}
}
exports.getAllCategory = async (req, res, next) => {
	try {
		// const { limit = 20, offset = 0 } = req.query
		const { category = '' } = req.query
		let catQuery = { isDeleted: false }
		if (mongoose.Types.ObjectId.isValid(category)) {
			catQuery = {
				_id: mongoose.Types.ObjectId(category),
				isDeleted: false
			}
		}
		console.log(catQuery)
		const categoryList = await Category.find({ ...catQuery }).sort('-createdAt')
		return res.json({
			success: true,
			message: 'All Category Successfully fetched',
			data: {
				categoryList
			}
		})
	} catch (e) {
		console.log('Log: exports.listCatImages -> e', e)
		return next(e)
	}
}
