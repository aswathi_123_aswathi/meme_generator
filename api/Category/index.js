const express = require('express')
const router = express.Router()

const categoryController = require('./controller')

router.post('/', categoryController.create)
router.get('/listImages', categoryController.listCatImages)
router.get('/list', categoryController.getAllCategory)
router.get('/count', categoryController.getCount)
router.put('/softDelete', categoryController.softDelete)

module.exports = router
