const mongoose = require('mongoose')
const Joi = require('@hapi/joi')

const { Schema } = mongoose

const AmountSchema = new Schema({
	defaultAmount: { type: Number, default: 4.99 }
})

AmountSchema.statics.validate = function(body) {
	const schema = {
		defaultAmount: Joi.string().required()
	}
	return Joi.validate(body, schema, {
		convert: true,
		abortEarly: false,
		allowUnknown: true
	})
}

module.exports = mongoose.model('defaultAmount', AmountSchema)
