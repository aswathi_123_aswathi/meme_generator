const express = require('express')
const router = express.Router()

const defaultAmountController = require('./controller')

router.get('/getDefaultAmount', defaultAmountController.getDefaultAmount)
router.put('/changeDefaultAmount', defaultAmountController.changeDefaultAmount)

module.exports = router
