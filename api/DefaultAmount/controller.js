const Amount = require('./model')

exports.changeDefaultAmount = async (req, res, next) => {
	try {
		const { amount } = req.body
		if (!amount) {
			return res.json({
				success: false,
				message: 'Amount not found',
				data: {}
			})
		}
		console.log('Log: exports.changeDefaultAmount -> amount', amount)
		const defaultAmount = await Amount.findOne({})
		console.log('Log: exports.getDefaultAmount -> defaultAmount', defaultAmount)
		defaultAmount.defaultAmount = amount
		await defaultAmount.save()
		return res.json({
			success: true,
			message: 'Successfully updated',
			data: {
				defaultAmount: defaultAmount.toObject().defaultAmount
			}
		})
	} catch (e) {
		console.log(e)
		return next(e)
	}
}

exports.getDefaultAmount = async (req, res, next) => {
	try {
		const defaultAmount = await Amount.findOne({})
		console.log('Log: exports.getDefaultAmount -> defaultAmount', defaultAmount)
		return res.json({
			success: true,
			message: 'Default Amount Successfully fetched',
			data: {
				defaultAmount: defaultAmount.toObject().defaultAmount
			}
		})
	} catch (e) {
		console.log('Log: exports.getDefaultAmount -> e', e)
		return next(e)
	}
}
