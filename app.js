const express = require('express')
const path = require('path')
const cors = require('cors')
const helmet = require('helmet')
const createError = require('http-errors')
const logger = require('morgan')
require('./db')
const proxy = require('html2canvas-proxy');
const app = express()

app.use(cors())
app.use(helmet())
app.use('/', proxy());
app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(express.static(path.join(__dirname, 'public')))
app.all('/api/*', [require('./middlewares/auth')])

app.use('/', require('./routes'))

// catch 404 and forward to error handler
app.use((req, res, next) => next(createError(404)))

// error handler
app.use(require('./helpers/server-error'))

const port = process.env.PORT
app.listen(port, () => console.log(`App listening at port ${port}`))
