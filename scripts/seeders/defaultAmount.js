
require('../../db')

const User = require('../../api/User/model')
const DefaultAmount = require('../../api/DefaultAmount/model')
;(async () => {
	try {
		await DefaultAmount.create({
			defaultAmount: 4.99
		})
		console.log('Data successfully seeded')
		process.exit(0)
	} catch (e) {
		throw Error(e)
	}
})()
