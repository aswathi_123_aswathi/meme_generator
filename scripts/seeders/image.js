require('../../db')

const Image = require('../../api/CatImages/model')

;(async () => {
	try {
		const img = {
			name: '1.jpg',
			categoryId: '5e68df9ae4559b14b6bd1eb5',
			path: 'http://localhost:5000/uploads/1.jpg'
		}

		await Image.create(img)
		console.log('Data successfully seeded')
		process.exit(0)
	} catch (e) {
		throw Error(e)
	}
})()
