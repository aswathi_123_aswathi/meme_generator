require('../../db')

const User = require('../../api/User/model')
const DefaultAmount = require('../../api/DefaultAmount/model')
;(async () => {
	try {
		const password = await User.generateHash('Ecks9623')

		// const role = await Role.findOne({ name: "Innago Admin" });

		await User.create({
			email: 'info@esimchavites.com',
			password,
			firstname: 'esimchavites',
			lastname: 'admin'
		})

		await DefaultAmount.create({
			defaultAmount: 4.99
		})
		console.log('Data successfully seeded')
		process.exit(0)
	} catch (e) {
		throw Error(e)
	}
})()
