const express = require('express')

const router = express.Router()

router.get('/', (req, res, next) => res.send('Welcome to Meme Generator'))
router.use('/auth', require('../api/Auth'))
router.use('/api/category', require('../api/Category'))
router.use('/api/meme', require('../api/Meme'))
router.use('/user', require('../api/User'))
router.use('/api/admin', require('../api/Admin'))
router.use('/api/transaction', require('../api/Transactions'))
router.use('/api/font', require('../api/Font'))


module.exports = router
