const jwt = require('jsonwebtoken')

const {
	jwt: { secretKey }
} = require('../config')

module.exports = (req, res, next) => {
	try {
		const token = req.header('Authorization').replace('Bearer ', '')
		console.log('token--------', token)
		if (!token) {
			return res.status(401).json({
				status: false,
				message: 'Access denied. No token provided.'
			})
		}
		const decoded = jwt.verify(token, secretKey)
		if (decoded.exp * 1000 <= Date.now()) {
			return res.status(401).json({
				status: false,
				message: 'Token Expired'
			})
		}
		req.user = decoded
		// return res.status(400).json({
		// 	user: req.user,
		// 	message: 'Token Expired',
		// });
		return next()
	} catch (ex) {
		return res.status(401).json({
			success: false,
			message: 'Invalid Token.'
		})
	}
}
