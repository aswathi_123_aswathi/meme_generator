/* eslint-disable no-else-return */
const multer = require('multer')
const path = require('path')
const { unlinkFiles } = require('../modules/fileOperations')

const fileSettings = {
	image: {
		maxSize: 10
	},
	video: {
		maxSize: 200
	}
}
const storage = multer.diskStorage({
	destination: './public/uploads/userSubmission/',
	filename(req, file, cb) {
		console.log('--------')

		console.log('---test', file)
		const fileType = file.mimetype.split('/')
		console.log('Log: filename -> fileType', fileType)
		if (
			(fileType[0] === 'image' && file.fieldname === 'file') ||
			(fileType[0] === 'image' && file.fieldname === 'meme') ||
			(fileType[0] === 'application' && file.fieldname === 'file') ||
			(fileType[0] === 'image' && file.fieldname === 'meme') ||
			(fileType[0] === 'application' && file.fieldname === 'meme')
		) {
			return cb(null, `image_${Date.now()}${path.extname(file.originalname)}`)
		} else if (
			fileType[0] === 'video' &&
			file.fieldname === 'video' &&
			['quicktime', 'mp4'].includes(fileType[1])
		) {
			return cb(
				null,
				`videos/video_${Date.now()}${path.extname(file.originalname)}`
			)
		}
		const errorMsg = 'Invalid file type'
		return cb(errorMsg)
	}
})

const upload = multer({
	storage

	//file filter
	// fileFilter: function(req, file, cb) {
	// 	checkFileType(file, cb)
	// }
}).fields([
	{
		name: 'file',
		maxCount: 6
	},
	{
		name: 'meme',
		maxCount: 6
	},
	{
		name: 'video',
		maxCount: 1
	}
])

// custom error handling middleware for file upload
module.exports = async (req, res, next) => {
	const files = []
	try {
		upload(req, res, err => {
			console.log('files-------', req.files)
			if (req.files) {
				let validationError = err || ''
				const { file, video, meme } = req.files
				// file size validation
				meme &&
					meme.some(img => {
						const imageSize = img.size / (1024 * 1024)
						files.push(img)
						if (imageSize > fileSettings.image.maxSize) {
							validationError = 'Image size should not be greater than 10MB'
							return true
						}
						return false
					})
				file &&
					file.some(img => {
						const imageSize = img.size / (1024 * 1024)
						files.push(img)
						if (imageSize > fileSettings.image.maxSize) {
							validationError = 'Image size should not be greater than 10MB'
							return true
						}
						return false
					})
				video &&
					video.some(vid => {
						const videoSize = vid.size / (1024 * 1024)
						files.push(vid)
						if (videoSize > fileSettings.video.maxSize) {
							validationError = 'Video size should not be greater than 200MB'
							return true
						}
						return false
					})
				if (validationError) {
					unlinkFiles(files)
					return res.json({
						STATUS: false,
						MSG: validationError,
						RESULT: ''
					})
				}
			}
			next()
		})
	} catch (error) {
		console.log(error)

		unlinkFiles(files)
		res.locals['responseFormat'] = 'json'
		return next(error)
	}
}
// function checkFileType(file, cb) {
// 	//Allowed Extensions
// 	const fileTypes = /jpeg|jpg|png/
// 	//Check Extension
// 	const extname = fileTypes.test(path.extname(file.originalname).toLowerCase())
// 	console.log('Log: checkFileType -> extname', extname)
// 	//Check mime
// 	const mimetype = fileTypes.test(file.mimetype)
// 	console.log('Log: checkFileType -> mimetype', mimetype)

// 	if (mimetype && extname) {
// 		return cb(null, true)
// 	} else {
// 		cb('Error! Only Images')
// 	}
// }
